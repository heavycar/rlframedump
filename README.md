# rlframedump

```
rlframedump
Parses a framedump and outputs a csv.

USAGE:
    rlframedump [FLAGS] [OPTIONS] [dump]

FLAGS:
    -h, --help       
            Prints help information

    -s, --silent     
            Hide all log output except for errors

    -V, --version    
            Prints version information

    -v, --verbose    
            Pass many times for more log output
            
            By default, it'll report info. Passing `-v` one time also prints debug, `-vv` enables trace.

OPTIONS:
    -o, --output <output>    
            Optional path to the output file or directory. Defaults to the source directory.
            
            If - is passed, outputs to stdout.

ARGS:
    <dump>    
            Path to the frame dump file to parse.
            
            Uses stdin if not specified.
```

## Building

```bash
cargo build --release
```