use failure::Fail;
use std::{
    fmt,
    io,
    num::{ParseFloatError, ParseIntError},
};

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "IO error: {:?}", _0)]
    Io(#[cause] io::Error),
    #[fail(display = "Invalid line")]
    InvalidLine,
    #[fail(
        display = "Parse error: {{ column: {}, line: {}, cause: {} }}",
        column, line, cause
    )]
    Parse {
        column: String,
        line: String,
        #[cause]
        cause: Box<Error>,
    },
    #[fail(display = "Float parse error: {}", _0)]
    ParseFloat(#[cause] ParseFloatError),
    #[fail(display = "Int parse error: {}", _0)]
    ParseInt(#[cause] ParseIntError),
    #[fail(display = "Vec3 parse error: {}", _0)]
    ParseVec3(String),
    #[fail(display = "Invalid physics step")]
    InvalidStep,
    #[fail(display = "Format error: {}", _0)]
    Fmt(#[cause] fmt::Error),
    #[fail(display = "Csv error: {}", _0)]
    Csv(#[cause] csv::Error),
}

pub type RLResult<T = ()> = Result<T, Error>;

impl Fail for Box<Error> {}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Error {
        Error::Io(e)
    }
}

impl From<ParseFloatError> for Error {
    fn from(e: ParseFloatError) -> Error {
        Error::ParseFloat(e)
    }
}

impl From<ParseIntError> for Error {
    fn from(e: ParseIntError) -> Error {
        Error::ParseInt(e)
    }
}

impl From<fmt::Error> for Error {
    fn from(e: fmt::Error) -> Error {
        Error::Fmt(e)
    }
}

impl From<csv::Error> for Error {
    fn from(e: csv::Error) -> Error {
        Error::Csv(e)
    }
}
