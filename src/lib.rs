use serde::{Deserialize, Serialize};
use std::{
    fmt::{self, Write as FmtWrite},
    fs::File,
    io::{self, BufRead, BufReader, Read, Write},
    iter,
    ops::Sub,
    path::Path,
    str::FromStr,
};

pub const CSV_HEADER: &str = "FrameNum,Time,Elapsed,MaxFPS,DesiredSleep,ActualSleep,Oversleep,DeltaTime,LX,LY,RunFrames,LeftoverTime,VT,VS,VP,VY,VR,VDF,VDS,VB,VHB,VJ,VelX,VelY,VelZ,AngVelX,AngVelY,AngVelZ,StickyX,StickyY,StickyZ,Total,Game,Render,GPU";

mod errors;

pub use errors::{Error, RLResult};

#[derive(Debug, Copy, Clone, PartialEq, PartialOrd, Default, Serialize, Deserialize)]
pub struct Vec3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Sub for Vec3 {
    type Output = Vec3;

    fn sub(self, rhs: Vec3) -> Vec3 {
        Vec3 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl FromStr for Vec3 {
    type Err = Error;

    fn from_str(s: &str) -> RLResult<Vec3> {
        let err = || Error::ParseVec3(s.to_owned());
        let mut split = s.split(' ');
        let x = parse_f64(split.next().ok_or_else(err)?)?;
        let y = parse_f64(split.next().ok_or_else(err)?)?;
        let z = parse_f64(split.next().ok_or_else(err)?)?;
        Ok(Vec3 { x, y, z })
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Default, Serialize, Deserialize)]
pub struct RBState {
    pub location: Vec3,
    pub rotation: Vec3,
    pub linear_velocity: Vec3,
    pub angular_velocity: Vec3,
}

impl FromStr for RBState {
    type Err = Error;

    fn from_str(s: &str) -> RLResult<RBState> {
        let location = column_slice(s, "Loc")?.parse()?;
        let rotation = column_slice(s, "Rot")?.parse()?;
        let linear_velocity = column_slice(s, "LinVel")?.parse()?;
        let angular_velocity = column_slice(s, "AngVel")?.parse()?;

        Ok(RBState {
            location,
            rotation,
            linear_velocity,
            angular_velocity,
        })
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Step {
    pub physics_frame: usize,
    pub timestep: f64,
    pub car_pre_phys: RBState,
    pub car_post_phys: RBState,
    pub sticky: Vec3,
}

impl Default for Step {
    fn default() -> Step {
        Step {
            physics_frame: 0,
            timestep: 8.333,
            car_pre_phys: Default::default(),
            car_post_phys: Default::default(),
            sticky: Default::default(),
        }
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Frame {
    pub index: usize,
    pub time: f64,
    pub elapsed: f64,
    pub max_fps: usize,
    pub desired_sleep: f64,
    pub actual_sleep: f64,
    pub delta_time: f64,
    pub lx: f64,
    pub ly: f64,
    pub run_frames: usize,
    pub leftover_time: f64,
    pub vt: f64,
    pub vs: f64,
    pub vp: f64,
    pub vy: f64,
    pub vr: f64,
    pub vdf: f64,
    pub vds: f64,
    pub vb: u8,
    pub vhb: u8,
    pub vj: u8,
    pub steps: Vec<Step>,
    pub latest_step: Step,
    pub total: f64,
    pub game: f64,
    pub render: f64,
    pub gpu: f64,
}

impl Frame {
    fn parse_line(&mut self, line: &str) -> RLResult<bool> {
        if line.starts_with("Start:") {
            self.index = column_slice(line, "Frame")?.parse()?;
            self.time = parse_f64(column_slice(line, "Time")?)?;
            self.elapsed = parse_f64(column_slice(line, "Elapsed")?)?;
            self.max_fps = column_slice(line, "MaxFPS")?.parse()?;
            self.desired_sleep = parse_f64(column_slice(line, "Wait")?)?;
            self.actual_sleep = parse_f64(column_slice(line, "Slept")?)?;
            self.delta_time = parse_f64(column_slice(line, "DeltaTime")?)?;

            self.run_frames = 0;
        } else if line.starts_with("Input:") {
            if let Ok(lx) = column_slice(line, "LX") {
                self.lx = parse_f64(lx)?;
            };
            if let Ok(ly) = column_slice(line, "LY") {
                self.ly = parse_f64(ly)?;
            };
        } else if line.starts_with("Physics:") {
            self.run_frames = column_slice(line, "RunFrames")?.parse()?;
            self.leftover_time = parse_f64(column_slice(line, "LeftoverTime")?)?;
        } else if line.starts_with("Vehicle:") {
            self.vt = parse_f64(column_slice(line, "T")?)?;
            self.vs = parse_f64(column_slice(line, "S")?)?;
            self.vp = parse_f64(column_slice(line, "P")?)?;
            self.vy = parse_f64(column_slice(line, "Y")?)?;
            self.vr = parse_f64(column_slice(line, "R")?)?;
            self.vdf = parse_f64(column_slice(line, "DF")?)?;
            self.vds = parse_f64(column_slice(line, "DS")?)?;
            self.vb = column_slice(line, "B")?.parse()?;
            self.vhb = column_slice(line, "HB")?.parse()?;
            self.vj = column_slice(line, "J")?.parse()?;
        } else if line.starts_with("Step:") {
            let step = Step {
                physics_frame: column_slice(line, "PhysicsFrame")?.parse()?,
                timestep: parse_f64(column_slice(line, "Time")?)?,
                ..Default::default()
            };
            self.steps.push(step);
        } else if line.starts_with("Pre Phys (Car)") || line.starts_with("Pre Phys: Car") {
            let step = self.steps.last_mut().ok_or(Error::InvalidStep)?;
            step.car_pre_phys = line.parse()?;
        } else if line.starts_with("Pos Phys (Car)") || line.starts_with("Pos Phys: Car") {
            let step = self.steps.last_mut().ok_or(Error::InvalidStep)?;
            step.car_post_phys = line.parse()?;
        } else if line.starts_with("Force (Car): Sticky") {
            let step = self.steps.last_mut().ok_or(Error::InvalidStep)?;
            step.sticky = column_slice(line, "World")?.parse()?;
        } else if line.starts_with("Frame:") {
            self.total = parse_f64(column_slice(line, "Total")?)?;
            self.game = parse_f64(column_slice(line, "Game")?)?;
            self.render = parse_f64(column_slice(line, "Render")?)?;
            self.gpu = parse_f64(column_slice(line, "GPU")?)?;

            return Ok(true);
        }

        Ok(false)
    }
}

impl fmt::Display for Frame {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}",
            self.index,
            self.time,
            self.elapsed,
            self.max_fps,
            self.desired_sleep,
            self.actual_sleep,
            (self.actual_sleep - self.desired_sleep).max(0.0),
            self.delta_time,
            self.lx,
            self.ly,
            self.run_frames,
            self.leftover_time,
            self.vt,
            self.vs,
            self.vp,
            self.vy,
            self.vr,
            self.vdf,
            self.vds,
            self.vb,
            self.vhb,
            self.vj,
            self.latest_step.car_pre_phys.linear_velocity.x,
            self.latest_step.car_pre_phys.linear_velocity.y,
            self.latest_step.car_pre_phys.linear_velocity.z,
            self.latest_step.car_post_phys.angular_velocity.x,
            self.latest_step.car_post_phys.angular_velocity.y,
            self.latest_step.car_post_phys.angular_velocity.z,
            self.latest_step.sticky.x,
            self.latest_step.sticky.y,
            self.latest_step.sticky.z,
            self.total,
            self.game,
            self.render,
            self.gpu,
        )
    }
}

impl AsRef<Frame> for Frame {
    fn as_ref(&self) -> &Frame {
        self
    }
}

fn column_slice<'a>(line: &'a str, column: &str) -> RLResult<&'a str> {
    let l_err = || Error::Parse {
        column: column.to_owned(),
        line: line.to_owned(),
        cause: Box::new(Error::InvalidLine),
    };
    let mut column = column.to_owned();
    column.push('=');

    let start = line.find(&column).ok_or_else(l_err)?;
    let line = &line[start + column.len()..];
    let end = match line.find('=') {
        Some(next) => line[..next].rfind(' ').ok_or_else(l_err)?,
        None => line.len(),
    };

    Ok(&line[..end])
}

fn parse_f64(string: &str) -> RLResult<f64> {
    string
        .trim_matches(|c: char| !(c.is_numeric() || c == '-'))
        .parse()
        .map_err(Error::from)
}

pub fn parse_read<R: BufRead>(mut reader: R) -> impl Iterator<Item = RLResult<Frame>> {
    let mut line = String::new();
    let mut frame = Frame::default();

    iter::repeat_with(move || loop {
        line.clear();
        match reader.read_line(&mut line) {
            Ok(bytes) => {
                if bytes == 0 {
                    return None;
                }
            }
            Err(e) => return Some(Err(e.into())),
        };
        if line.ends_with('\n') {
            let trim_len = line.trim_end().len();
            line.truncate(trim_len);
        }
        let frame_complete = match frame.parse_line(&line) {
            Ok(fc) => fc,
            Err(e) => {
                return Some(Err(Error::Parse {
                    line: line.to_owned(),
                    column: line.to_owned(),
                    cause: Box::new(e),
                }));
            }
        };
        if frame_complete {
            if let Some(step) = frame.steps.last() {
                frame.latest_step = step.clone();
            }
            let ret = frame.clone();
            frame.steps.clear();
            return Some(Ok(ret));
        }
    })
    .take_while(Option::is_some)
    .map(Option::unwrap)
}

pub fn parse_file<P: AsRef<Path>>(path: P) -> RLResult<impl Iterator<Item = RLResult<Frame>>> {
    Ok(parse_read(BufReader::new(File::open(path)?)))
}

pub fn parse_str<'a>(s: &'a str) -> impl Iterator<Item = RLResult<Frame>> + 'a {
    parse_read(io::Cursor::new(s.as_bytes()))
}

/// An iterator that produces only the `T` values as long as the
/// inner iterator produces `Ok(T)`.
///
/// Copied from `itertools`.
#[derive(Debug)]
pub struct ProcessResults<'a, I, E: 'a> {
    error: &'a mut Result<(), E>,
    iter: I,
}

impl<'a, I, T, E> Iterator for ProcessResults<'a, I, E>
where
    I: Iterator<Item = Result<T, E>>,
{
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        match self.iter.next() {
            Some(Ok(x)) => Some(x),
            Some(Err(e)) => {
                *self.error = Err(e);
                None
            }
            None => None,
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let (_, hi) = self.iter.size_hint();
        (0, hi)
    }
}

pub fn lift_frames<F, I, P, R>(frames: I, f: P) -> RLResult<R>
where
    F: AsRef<Frame>,
    I: IntoIterator<Item = RLResult<F>>,
    P: FnOnce(ProcessResults<I::IntoIter, Error>) -> R,
{
    let iter = frames.into_iter();
    let mut error = Ok(());

    let result = f(ProcessResults {
        error: &mut error,
        iter,
    });

    error.map(|_| result)
}

pub fn frames_to_csv_string<F: AsRef<Frame>, I: IntoIterator<Item = F>>(frames: I) -> String {
    let mut s = String::new();
    let _ = writeln!(&mut s, "{}", CSV_HEADER);

    for frame in frames {
        let _ = writeln!(&mut s, "{}", frame.as_ref());
    }

    s
}

pub fn frames_to_csv_writer<F: AsRef<Frame>, I: IntoIterator<Item = F>, W: Write>(
    frames: I,
    mut writer: W,
) -> RLResult {
    writeln!(&mut writer, "{}", CSV_HEADER)?;

    for frame in frames {
        writeln!(&mut writer, "{}", frame.as_ref())?;
    }
    writer.flush()?;

    Ok(())
}

pub fn frames_res_to_csv_string<F: AsRef<Frame>, I: IntoIterator<Item = RLResult<F>>>(
    frames: I,
) -> RLResult<String> {
    #[allow(clippy::redundant_closure)]
    lift_frames(frames, |f| frames_to_csv_string(f))
}

pub fn frames_res_to_csv_writer<F: AsRef<Frame>, I: IntoIterator<Item = RLResult<F>>, W: Write>(
    frames: I,
    writer: W,
) -> RLResult {
    lift_frames(frames, |f| frames_to_csv_writer(f, writer))?
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct CsvFrame {
    pub frame_num: usize,
    pub time: f64,
    pub elapsed: f64,
    #[serde(rename = "MaxFPS")]
    pub max_fps: usize,
    pub desired_sleep: f64,
    pub actual_sleep: f64,
    pub delta_time: f64,
    #[serde(rename = "LX")]
    pub lx: f64,
    #[serde(rename = "LY")]
    pub ly: f64,
    pub run_frames: usize,
    pub leftover_time: f64,
    #[serde(rename = "VT")]
    pub vt: f64,
    #[serde(rename = "VS")]
    pub vs: f64,
    #[serde(rename = "VP")]
    pub vp: f64,
    #[serde(rename = "VY")]
    pub vy: f64,
    #[serde(rename = "VR")]
    pub vr: f64,
    #[serde(rename = "VDF")]
    pub vdf: f64,
    #[serde(rename = "VDS")]
    pub vds: f64,
    #[serde(rename = "VB")]
    pub vb: u8,
    #[serde(rename = "VHB")]
    pub vhb: u8,
    #[serde(rename = "VJ")]
    pub vj: u8,
    pub vel_x: f64,
    pub vel_y: f64,
    pub vel_z: f64,
    pub ang_vel_x: f64,
    pub ang_vel_y: f64,
    pub ang_vel_z: f64,
    pub sticky_x: f64,
    pub sticky_y: f64,
    pub sticky_z: f64,
    pub total: f64,
    pub game: f64,
    pub render: f64,
    #[serde(rename = "GPU")]
    pub gpu: f64,
}

impl From<CsvFrame> for Frame {
    fn from(csv_frame: CsvFrame) -> Frame {
        let step = Step {
            car_pre_phys: RBState {
                linear_velocity: Vec3 {
                    x: csv_frame.vel_x,
                    y: csv_frame.vel_y,
                    z: csv_frame.vel_z,
                },
                ..RBState::default()
            },
            car_post_phys: RBState {
                angular_velocity: Vec3 {
                    x: csv_frame.ang_vel_x,
                    y: csv_frame.ang_vel_y,
                    z: csv_frame.ang_vel_z,
                },
                ..RBState::default()
            },
            sticky: Vec3 {
                x: csv_frame.sticky_x,
                y: csv_frame.sticky_y,
                z: csv_frame.sticky_z,
            },
            ..Default::default()
        };

        Frame {
            index: csv_frame.frame_num,
            time: csv_frame.time,
            elapsed: csv_frame.elapsed,
            max_fps: csv_frame.max_fps,
            desired_sleep: csv_frame.desired_sleep,
            actual_sleep: csv_frame.actual_sleep,
            delta_time: csv_frame.delta_time,
            lx: csv_frame.lx,
            ly: csv_frame.ly,
            run_frames: csv_frame.run_frames,
            leftover_time: csv_frame.leftover_time,
            vt: csv_frame.vt,
            vs: csv_frame.vs,
            vp: csv_frame.vp,
            vy: csv_frame.vy,
            vr: csv_frame.vr,
            vdf: csv_frame.vdf,
            vds: csv_frame.vds,
            vb: csv_frame.vb,
            vhb: csv_frame.vhb,
            vj: csv_frame.vj,
            steps: if csv_frame.run_frames > 0 {
                vec![step.clone()]
            } else {
                vec![]
            },
            latest_step: step,
            total: csv_frame.total,
            game: csv_frame.game,
            render: csv_frame.render,
            gpu: csv_frame.gpu,
            ..Frame::default()
        }
    }
}

pub fn parse_csv_read<R: Read>(reader: R) -> impl Iterator<Item = RLResult<Frame>> {
    csv::Reader::from_reader(reader)
        .into_deserialize::<CsvFrame>()
        .map(|frame| frame.map(Frame::from).map_err(Error::from))
}

pub fn parse_csv_file<P: AsRef<Path>>(path: P) -> RLResult<impl Iterator<Item = RLResult<Frame>>> {
    Ok(parse_csv_read(BufReader::new(File::open(path)?)))
}

pub fn parse_csv_str<'a>(s: &'a str) -> impl Iterator<Item = RLResult<Frame>> + 'a {
    parse_csv_read(io::Cursor::new(s.as_bytes()))
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::f64;

    const TEST_LOG: &str = include_str!("../framedump.bin.log");
    //    const COMPACT_TEST_LOG: &str = include_str!("../compact_framedump.bin.log");
    const TEST_CSV: &str = include_str!("../ref2_framedump.bin.log.csv");

    fn f64_eq(lhs: f64, rhs: f64) -> bool {
        rhs <= lhs + f64::EPSILON && rhs >= lhs - f64::EPSILON
    }

    #[test]
    fn parses_column() {
        let line = "Physics: DeltaTime=6.667ms AccumTime=11.122ms AccumFrames=1 RunFrames=1 LeftoverTime=2.789ms";
        let assert_col = |col, val| assert_eq!(column_slice(line, col).unwrap(), val);

        assert_col("DeltaTime", "6.667ms");
        assert_col("AccumTime", "11.122ms");
        assert_col("AccumFrames", "1");
        assert_col("RunFrames", "1");
        assert_col("LeftoverTime", "2.789ms");
    }

    #[test]
    fn parses_f64() {
        assert!(f64_eq(parse_f64("1231.31").unwrap(), 1231.31));
        assert!(f64_eq(parse_f64("11.122ms").unwrap(), 11.122));
    }

    #[test]
    fn parses_vec3() {
        let vec: Vec3 = "-0.091 0.000 8.311".parse().unwrap();
        assert_eq!(
            vec,
            Vec3 {
                x: -0.091,
                y: 0.0,
                z: 8.311
            }
        );
    }

    #[test]
    fn parses_rbstate() {
        let s = "Loc=-53.390 -1977.030 17.010 Rot=0.000 -0.555 90.505 LinVel=-0.091 0.000 8.311 AngVel=-0.000 -0.001 -0.000";
        let rbs: RBState = s.parse().unwrap();
        assert_eq!(
            rbs,
            RBState {
                location: Vec3 {
                    x: -53.390,
                    y: -1977.030,
                    z: 17.010
                },
                rotation: Vec3 {
                    x: 0.0,
                    y: -0.555,
                    z: 90.505
                },
                linear_velocity: Vec3 {
                    x: -0.091,
                    y: 0.0,
                    z: 8.311
                },
                angular_velocity: Vec3 {
                    x: -0.0,
                    y: -0.001,
                    z: -0.0
                },
            }
        )
    }

    #[test]
    fn parses_str() {
        let csv_str = frames_res_to_csv_string(parse_str(TEST_LOG)).unwrap();
        for (gen_line, ref_line) in csv_str.lines().zip(TEST_CSV.lines()) {
            assert_eq!(gen_line, ref_line);
        }

        let mut csv_buf = Vec::new();
        {
            let writer = io::Cursor::new(&mut csv_buf);
            frames_res_to_csv_writer(parse_str(TEST_LOG), writer).unwrap();
        }
        let csv_buf = unsafe { String::from_utf8_unchecked(csv_buf) };
        for (gen_line, ref_line) in csv_buf.lines().zip(TEST_CSV.lines()) {
            assert_eq!(gen_line, ref_line);
        }
    }

    #[test]
    fn parse_csv() -> Result<(), failure::Error> {
        parse_csv_str(TEST_CSV).count();
        Ok(())
    }

    //    #[test]
    //    #[ignore]
    //    fn parses_compact() {
    //        frames_res_to_csv_string(parse_str(COMPACT_TEST_LOG)).unwrap();
    //    }
}
