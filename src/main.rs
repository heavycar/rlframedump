use failure::err_msg;
use log::{error, info};
use std::{env, fs, io, path::PathBuf, process};
use structopt::StructOpt;

type FResult<T = ()> = Result<T, failure::Error>;

/// Parses a framedump and outputs a csv.
#[derive(StructOpt, Debug)]
#[structopt(author = "")]
struct Opt {
    /// Path to the frame dump file to parse.
    ///
    /// Uses stdin if not specified.
    dump: Option<PathBuf>,
    /// Optional path to the output file or directory. Defaults to the source directory.
    ///
    /// If - is passed, outputs to stdout.
    #[structopt(short = "o", long = "output")]
    output: Option<PathBuf>,
    /// Pass many times for more log output
    ///
    /// By default, it'll report info. Passing `-v` one time also prints
    /// debug, `-vv` enables trace.
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: u8,
    /// Hide all log output except for errors
    #[structopt(short = "s", long = "silent")]
    silent: bool,
}

fn run() -> FResult {
    let opt: Opt = Opt::from_args();

    let write_stdout = match opt.output.as_ref() {
        Some(p) => p.to_string_lossy() == "-",
        None => false,
    };

    let log_level = if opt.silent || write_stdout {
        simplelog::LevelFilter::Error
    } else {
        match opt.verbose {
            0 => simplelog::LevelFilter::Info,
            1 => simplelog::LevelFilter::Debug,
            _ => simplelog::LevelFilter::Trace,
        }
    };
    simplelog::TermLogger::init(log_level, simplelog::Config::default())?;

    macro_rules! write_csv {
        ($iter:expr) => {
            if write_stdout {
                let stdout = io::stdout();
                let writer = stdout.lock();
                rlframedump::frames_res_to_csv_writer($iter, writer)?;
            } else {
                let out_filename = || -> FResult<String> {
                    match &opt.dump {
                        Some(dump) => Ok(dump
                            .file_name()
                            .ok_or_else(|| err_msg("Invalid dump path."))?
                            .to_string_lossy()
                            .into_owned()
                            + ".csv"),
                        None => Ok("framedump.bin.log.csv".to_owned()),
                    }
                };
                let out_path = match &opt.output {
                    Some(output) if output.is_dir() => output.join(out_filename()?),
                    Some(output) => output.to_owned(),
                    None => match &opt.dump {
                        Some(dump) => dump
                            .parent()
                            .ok_or_else(|| err_msg("Dump had no parent directory."))?
                            .join(out_filename()?),
                        None => env::current_dir()?.join(out_filename()?),
                    },
                };
                info!("Writing to {}", out_path.to_string_lossy());
                let writer = io::BufWriter::new(fs::File::create(out_path)?);
                rlframedump::frames_res_to_csv_writer($iter, writer)?;
            }
        };
    }
    if let Some(dump) = &opt.dump {
        info!("Parsing {}", dump.to_string_lossy());
        let frames = rlframedump::parse_file(dump)?;
        write_csv!(frames);
    } else {
        let stdin = io::stdin();
        let frames = rlframedump::parse_read(stdin.lock());
        write_csv!(frames);
    };

    info!("Done");
    Ok(())
}

fn main() {
    let res = match std::panic::catch_unwind(run) {
        Ok(r) => r,
        Err(e) => {
            error!("Panic: {:?}", e);
            process::exit(1);
        }
    };

    if let Err(e) = res {
        error!("{}", e);
        for cause in e.iter_causes() {
            error!("Cause:\n\t{}", cause);
        }
        let code = e
            .downcast_ref::<io::Error>()
            .and_then(io::Error::raw_os_error)
            .unwrap_or(1);
        process::exit(code);
    }
}
